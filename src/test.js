var SubwayLine = require('./line')

class SubwayFactory {
  constructor () {
    this.lines = []
    this.colors = ['RED', 'GREEN', 'BLUE', 'YELLOW', 'DARKBLUE', 'PINK']
  }

  addLine () {
    if (this.colors.length > 0) {
      let color = this.colors.pop()
      let line = new SubwayLine(color, this.genStartPos())
      this.lines.push(line)
    }
  }

  genStartPos () {
    let x = parseInt(Math.random() * 50) - 25
    let y = parseInt(Math.random() * 50) - 25
    return { x: x, y: y }
  }

  simulate () {
    for (var i = 0; i < 10; i++) {
      for (let line of this.lines) {
        line.findDirection()
      }
    }
  }
}

var Factory = (function () {
  var instance
  function createInstance () {
    var factory = new SubwayFactory()
    return factory
  }

  return {
    getInstance: function () {
      if (!instance) {
        instance = createInstance()
      }
      return instance
    }
  }
})()

var line = new SubwayLine('GREEN', { x: 0, y: 0 })
console.log(line.currentPos)
for (var i = 0; i < 10; i++) {
  line.chooseNextSquare()
  console.log(line.currentPos)
}
