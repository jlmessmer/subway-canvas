import { SubwayFactory } from './line'

(function () {
  var canvas = document.getElementById('lines')
  var ctx = canvas.getContext('2d')

  var factory = new SubwayFactory({ x: window.innerWidth, y: window.innerHeight })
  factory.generateSubway(8)
  var tracks = factory.getLines()
  var points = calcWaypoints(tracks)

  // variable to hold how many frames have elapsed in the animation
  // t represents each waypoint along the path and is incremented in the animation loop
  var t = 1

  // resize the canvas to fill browser window dynamically
  window.addEventListener('resize', resizeCanvas, false)

  function resizeCanvas () {
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight

    /**
         * Your drawings need to be inside this function otherwise they will be reset when
         * you resize the browser window and the canvas goes will be cleared.
         */
    drawStuff()
  }
  resizeCanvas()

  function calcWaypoints (lines) {
    var waypoints = []
    for (var i = 0; i < lines.length; i++) {
      waypoints[i] = []
      for (var j = 1; j < lines[i].length; j++) {
        var pt0 = lines[i][j - 1]
        var pt1 = lines[i][j]
        var dx = pt1.x - pt0.x
        var dy = pt1.y - pt0.y
        for (var k = 0; k < 100; k++) {
          var x = pt0.x + dx * k / 100
          var y = pt0.y + dy * k / 100
          waypoints[i].push({ x: x, y: y })
        }
      }
    }
    return (waypoints)
  }

  function drawStuff () {
    if (t < points[0].length - 1) { window.requestAnimationFrame(drawStuff) }
    // draw a line segment from the last waypoint
    // to the current waypoint
    for (var i = 0; i < points.length; i++) {
      ctx.beginPath()
      ctx.moveTo(points[i][t - 1].x, points[i][t - 1].y)
      ctx.lineTo(points[i][t].x, points[i][t].y)
      ctx.lineWidth = 10
      ctx.lineCap = 'round'
      ctx.lineJoin = 'round'
      ctx.strokeStyle = factory.lines[i].color
      ctx.fillStyle = factory.lines[i].color
      ctx.stroke()
      if ((t - 1) % 100 === 0) {
        ctx.strokeStyle = '#000000'
        ctx.fillStyle = '#000000'
        ctx.arc(points[i][t - 1].x, points[i][t - 1].y, 10, 0, 2 * Math.PI)
        ctx.stroke()
        ctx.fill()
      }
      if (t >= points[i].length - 1) {
        ctx.strokeStyle = '#000000'
        ctx.fillStyle = '#000000'
        ctx.arc(points[i][t].x, points[i][t].y, 10, 0, 2 * Math.PI)
        ctx.stroke()
        ctx.fill()
      }
    }
    // increment "t" to get the next waypoint
    t++
  }
})()
