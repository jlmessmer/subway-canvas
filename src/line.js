class SubwayLine {
  constructor (color, startPos, endPos, length, stations) {
    this.color = color
    this.startPos = startPos
    this.endPos = endPos
    this.track = new Array(length)
    this.track[0] = startPos
    this.track[length - 1] = endPos
    this.stations = stations
    this.stations.push(startPos)
    this.stations.push(endPos)
    this.generateTrack(0, length - 1, length)

    /*
    Chances of next jump going in a certain direction:
      left: 0.3,
      right: 0.3,
      straight: 0.3
    */
    this.directionOdds = {
      l: 0,
      r: 0,
      s: 1
    }
  }

  toString () {
    for (let stop of this.track) {
      return `(${stop.x}, ${stop.y}) -> `
    }
  }

  addStation (proposedLoc) {
    for (let station of this.stations) {
      var distance = Math.sqrt(Math.pow((station.x - proposedLoc.x), 2) + Math.pow((station.y - proposedLoc.y), 2))
      if (distance < 100) {
        console.log(`${JSON.stringify(proposedLoc)} - ${JSON.stringify(station)}`)
        console.log('Station exists!')
        return station
      }
    }
    this.stations.push(proposedLoc)
    return proposedLoc
  }

  generateTrack (start, end, length) {
    if (length > 2) {
      var startPos = this.track[start]
      var endPos = this.track[end]
      var proposedMidPos = {
        x: parseInt((endPos.x + startPos.x) / 2) + (parseInt(Math.random() * 200) - 100),
        y: parseInt((endPos.y + startPos.y) / 2) + (parseInt(Math.random() * 200) - 100)
      }
      var midPos = this.addStation(proposedMidPos)
      var mid = parseInt((start + end) / 2)
      this.track[mid] = midPos
      this.generateTrack(start, mid, mid - start + 1)
      this.generateTrack(mid, end, end - mid + 1)
    }
  }
}

class SubwayFactory {
  constructor (canvasSize) {
    this.lines = []
    this.stations = []
    this.colors = ['#006F11', '#113767', '#CF0000', '#6E9CFF', '#FF9300', '#613800', '#D8D200', '#4D005F']
    this.size = canvasSize
  }

  createLine () {
    if (this.colors.length > 1) {
      var startX = parseInt(Math.random() * this.size.x)
      var startY = (startX > this.size.x * 0.1 && startX < this.size.x * 0.9) ? parseInt(Math.random() * this.size.y * 0.1) : parseInt(Math.random() * this.size.y)

      var s = {
        x: startX,
        y: startY
      }

      var e = {
        x: this.size.x - s.x,
        y: this.size.y - s.y
      }

      var line = new SubwayLine(this.colors.pop(), s, e, 7, this.stations)
      this.lines.push(line)
    }
  }

  generateSubway (numLines = 4) {
    for (var i = 0; i < numLines; i++) {
      this.createLine()
    }
  }

  getLines () {
    var lines = []
    for (var i = 0; i < this.lines.length; i++) {
      lines[i] = this.lines[i].track
    }
    return lines
  }

  simulate () {
    for (let line of this.lines) {
      line.chooseNextSquare()
    }
  }

  print () {
    for (let line of this.lines) {
      console.log(line)
    }
  }
}

export { SubwayLine, SubwayFactory }
