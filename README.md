# Subway Canvas 
*A Javascript package to dynamically generate a subway map*

## About
Inspired by iconic maps of the London Underground and New York Subway, this
will Javascript library dynamically generates a map in a similar style.

## Using the library
1. Clone this repository
2. From the root directiory, install all required node modules: `npm install`
3. Build the project: `npm run build`
4. Include the `main.js` file from `./dist/` in your html


## Future Directions:
- Add some level of customization:
  - User control over how 'grid-like' the map is
  - User control over the number of lines
  - Modularity with which controls to include
- Build as React component

